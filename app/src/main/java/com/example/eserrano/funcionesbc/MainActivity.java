package com.example.eserrano.funcionesbc;

import android.content.Intent;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;
//import android.widget.EditText;
//import android.widget.Toast;

public class MainActivity extends AppCompatActivity
{
    TextInputLayout texto,texto2;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        texto = (TextInputLayout) findViewById(R.id.password);
        texto2 = (TextInputLayout) findViewById(R.id.texto);

        texto.getEditText().addTextChangedListener(new TextWatcher()
        {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2)
            {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2)
            {

            }

            @Override
            public void afterTextChanged(Editable editable)
            {
               //String cadena = texto.getEditText().toString();
                    if(editable.toString().length() < 8)
                    {
                        //Toast mensaje1  = Toast.makeText(getApplicationContext(),"Tiene que ser de 8 caracteres el texto!",Toast.LENGTH_LONG);
                        //mensaje1.show();
                        texto.setError("Error!");
                    }
                    else
                    {
                        texto.setErrorEnabled(false);
                        //Toast mensaje  = Toast.makeText(getApplicationContext(),"Número correcto...!",Toast.LENGTH_LONG);
                        //mensaje.show();

                        Intent newactivity = new Intent(getApplicationContext(), Activity_Emergente.class);
                        startActivity(newactivity);
                        finish();
                    }

            }
        });

        texto2.getEditText().addTextChangedListener(new TextWatcher()
        {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2)
            {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2)
            {

            }

            @Override
            public void afterTextChanged(Editable editable)
            {
                String cadena = editable.toString();
                if(cadena.charAt(0) == '/')
                {
                    texto2.setError("Encontro el slash");
                }
                else
                {
                    texto2.setError("No se encontro el slash!");
                }
            }
        });
    }
}
