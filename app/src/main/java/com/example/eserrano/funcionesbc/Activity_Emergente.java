package com.example.eserrano.funcionesbc;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

/**
 * Created by E.serrano on 5/17/18.
 */

public class Activity_Emergente extends AppCompatActivity
{
    Button btnRegresar;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_emergente);

        btnRegresar = (Button) findViewById(R.id.regresar);

        btnRegresar.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                Intent myactivity = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(myactivity);
                finish();

            }
        });
    }



}
